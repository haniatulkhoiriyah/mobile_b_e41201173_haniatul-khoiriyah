import 'package:flutter/material.dart';
import 'package:minggu_5_styling/screen.dart';

void main() {
runApp(MaterialApp(
 initialRoute: '/',
 routes: <String, WidgetBuilder>{
 '/': (context) => homePage(),
 '/about': (context) => AboutPage(),
 '/halaman-404' : (context) => ErrorPage(),
 },
));
}


class screen extends StatelessWidget {
  const screen({ Key? key }) : super(key: key);
  @override
  Widget build(BuildContext context) {
     return Scaffold(
 appBar: AppBar(
 title: Text('Belajar Routing'),
 ),
 body: Center(
 child: ElevatedButton(
 onPressed: () {
 Navigator.pushNamed(context, '/about');
 },
 child: Text('Tap Untuk ke AboutPage'),
 ),
 ),
 );
  }
}


class AboutPage extends StatelessWidget {
  const AboutPage({ Key? key }) : super(key: key);
 @override
  Widget build(BuildContext context) {
    return Scaffold(
 appBar: AppBar(
 title: Text('Tentang Aplikasi'),
 ),
 body: Center(
 child: ElevatedButton(
 onPressed: () {
 Navigator.pop(context);
 },
 child: Text('Kembali'),
 ),
 ),
 );
  }
}

