import 'package:flutter/material.dart';

class homePage extends StatelessWidget {
  const homePage({ Key? key }) : super(key: key);
@override
Widget build(BuildContext context) {
 return Scaffold(
 appBar: AppBar(
 title: Text('Belajar Routing'),
 ),
 body: Center(
 child: Column(
 mainAxisAlignment: MainAxisAlignment.center,
 children: [
 ElevatedButton(
 onPressed: () {
 Navigator.pushNamed(context, '/about');
 },
 child: Text('Tap Untuk ke AboutPage'),
 ),
 ElevatedButton(
 onPressed: () {
 Navigator.pushNamed(context, '/halaman-404');
 },
 child: Text('Tap Halaman lain'),
 ),
 ],
 ),
),
 );
}
}


class AboutPage extends StatelessWidget {
  const AboutPage({ Key? key }) : super(key: key);
@override
Widget build(BuildContext context) {
 return Scaffold(
 appBar: AppBar(
 title: Text('Tentang Aplikasi'),
 ),
 body: Center(
 child: ElevatedButton(
 onPressed: () {
 Navigator.pop(context);
 },
 child: Text('Kembali'),
 ),
 ),
 );
}
}

class ErrorPage extends StatelessWidget {
  const ErrorPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Error Page'),
      ),
      body: Center(child: Text('Error Page')),
    );
  }
}