class Product {
  final int id;
  final String productName;
  final String productionImage;
  final String productionDescription;
  final double price;

  Product(this.id, this.productName, this.productionImage, this.productionDescription,
  this.price);
}