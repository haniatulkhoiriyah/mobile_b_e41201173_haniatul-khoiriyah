import 'dart:async';

void main() {
  print("Cindai - Siti Nurhaliza");
  print("-------------------------------");
  print("");

  Timer(Duration(seconds: 21), () => print("Cindailah mana tidak berkias"));
  Timer(Duration(seconds: 26), () => print("Jalinnya lalu rentah beribu"));
  Timer(Duration(seconds: 32), () => print("Bagailah mana hendak berhias"));
  Timer(Duration(seconds: 36), () => print("Cerminku retak seribu"));
  Timer(Duration(seconds: 43), () => print("Mendendam unggas liar di hutan"));
  Timer(Duration(seconds: 48), () => print("Jalan yang tinggal jangan berliku"));
  Timer(Duration(seconds: 52), () => print("Tilamku emas cadarnya intan"));
  Timer(Duration(seconds: 57), () => print("Berbantal lengan tidurku"));
  Timer(Duration(seconds: 67), () => print("Hias cempaka kenanga tepian"));
  Timer(Duration(seconds: 73), () => print("Mekarnya kuntum nak idam kumbang"));
  Timer(Duration(seconds: 78), () => print("Puas ku jaga si bunga impian"));
  Timer(Duration(seconds: 82), () => print("Gugurnya sebelum berkembang"));
  Timer(Duration(seconds: 87), () => print("Hendaklah hendak hendak ku rasa (rasa sayang)"));
  Timer(Duration(seconds: 92), () => print("Puncaknya gunung hendak ditawan"));
  Timer(Duration(seconds: 97), () => print("Tidaklah tidak tidak ku daya"));
  Timer(Duration(seconds: 102), () => print("Tingginya tidak terlawan"));
}
