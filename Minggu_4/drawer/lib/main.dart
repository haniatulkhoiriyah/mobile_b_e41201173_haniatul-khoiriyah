import 'package:flutter/material.dart';
import "package:drawer/Telegram.dart";

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ChatYuk',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Telegram(),
    );
  }
}
