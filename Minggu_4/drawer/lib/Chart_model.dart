class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel(
      {required this.name,
      required this.message,
      required this.time,
      required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'Wafi',
      message: 'Hello Wafi',
      time: '12.00',
      profileUrl:
          'https://asset.kompas.com/crops/IjmmRwybxe-4tZrGRN5eMrq_po8=/0x32:700x382/750x500/data/photo/2020/01/09/5e16811892fc7.jpg'),
  ChartModel(
      name: 'Ajeng',
      message: 'Ajeng bergabung dengan ChatYuk',
      time: '19 Maret',
      profileUrl:
          'https://img.okezone.com/content/2019/04/30/298/2049787/chef-arnold-berbagi-tips-membuat-makanan-menjadi-lebih-enak-Tz2kVDuNL2.jpg'),
  ChartModel(
      name: 'Novita',
      message: 'Hello Novita',
      time: '10 Maret',
      profileUrl:
          'https://asset.kompas.com/crops/ployX7cQOqsYqJS2PYvUGv41CaI=/0x0:1000x667/750x500/data/photo/2017/06/22/163146320170622-042902-8311-chef.juna-.atau-.junior-.rorimpandey-.jpg'),
  ChartModel(
      name: 'Vina',
      message: 'Vina bergabung dengan ChatYuk',
      time: '9 Maret',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Anwar',
      message: 'Hello Anwar',
      time: '13 Februari',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Yana',
      message: 'Yana bergabung dengan ChatYuk',
      time: '12 Februari',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Zainal',
      message: 'Hello Zainal',
      time: '11 Februari',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Atul',
      message: 'Atul bergabung dengan ChatYuk',
      time: '24 Januari',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Khoiriyah',
      message: 'Hello Khoiriyah',
      time: '11 Januari',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
];
