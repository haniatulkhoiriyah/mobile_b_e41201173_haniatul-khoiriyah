import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset('assets/images/hani.jpg'),
        SizedBox(
          height: 30,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Column(children: [
            Text(
              'HANI ATUL KHOIRIYAH E41201173',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ]),
        ),
      ],
    )));
  }
}